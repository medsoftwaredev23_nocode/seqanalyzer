# SeqAnalyzer

The SeqAnalyzer is a Java program that analyzes the nucleotide composition of a sequence.

## Usage

To run the SeqAnalyzer program, follow these steps:

1. Clone the repository to your local machine:

   ```
   git clone https://gitlab.fhnw.ch/medsoftwaredev23_nocode/seqanalyzer/
   ```

2. Navigate to the `seqanalyzer` directory:

   ```
   cd seqanalyzer
   ```

3. Compile the source code using Ant:

   ```
   ant compile
   ```

4. Generate the distribution by running the following command:

   ```
   ant dist
   ```

   This will create a JAR file named `seqanalyzer-YYYYMMDD.jar` in the `dist/lib` directory.

5. Run the program by executing the JAR file with the path to your sequence FASTA file as a command line parameter:

   ```
   java -jar dist/lib/seqanalyzer-YYYYMMDD.jar sequencefile.fasta
   ```

   Replace `YYYYMMDD` with the actual date stamp in the JAR file name.

## Dependencies

The program requires the following dependencies:

- Java Development Kit (JDK)
- Apache Ant

Ensure that both the JDK and Ant are installed and properly configured on your system before running the SeqAnalyzer program.

## Build

If you want to modify and rebuild the SeqAnalyzer program, the project includes an Ant build script (`build.xml`) that automates the build process. The build script performs the following tasks:

- Compiles the source code
- Creates the necessary directory structure
- Generates the distribution JAR file

To build the project, execute the following command:

```
ant
```

This will compile the source code and generate the JAR file in the `dist/lib` directory.

## Cleaning Up

To clean up the project and remove the build artifacts, run the following command:

```
ant clean
```

This will delete the `build` and `dist` directories.

