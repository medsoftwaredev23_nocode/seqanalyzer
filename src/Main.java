import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.io.FastaReaderHelper;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.io.File;

public class Main {
    public static void main(String[] args) {

        // Load configuration file
        Properties props = new Properties();
        try {
            FileInputStream configStream = new FileInputStream("config.properties");
            props.load(configStream);
            configStream.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(0);
        }

        // Get input file path from configuration file
        String inputFilePath = props.getProperty("input_file");
        System.out.println("Input file path: " + inputFilePath);

        // Analyze file
        try {
            LinkedHashMap<String, DNASequence> seqMap = readFastaFile(inputFilePath);
            String sequence = null;
            Map<String, Integer> counterMap = new HashMap<>();

            for (String key : seqMap.keySet()) {
                sequence = seqMap.get(key).getSequenceAsString();
                for (int i = 0; i < sequence.length(); i++) {
                    String s = String.valueOf(sequence.charAt(i));
                    if (counterMap.containsKey(s)) {
                        Integer c = counterMap.get(s);
                        c++;
                        counterMap.put(s, c);
                    } else {
                        counterMap.put(s, 1);
                    }
                }
            }

            for (String key : counterMap.keySet()) {
                System.out.println(key + " : " + counterMap.get(key));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static LinkedHashMap<String, DNASequence> readFastaFile(String filename) throws Exception {
        LinkedHashMap<String, DNASequence> seq = FastaReaderHelper.readFastaDNASequence(new File(filename));
        return seq;
    }
}


